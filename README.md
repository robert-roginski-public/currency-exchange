<div style="text-align: center">
  <h1>Currency Exchange</h1>
</div>

### Sections
- [Install](#install)
- [Makefile](#makefile)
- [How to use](#how-to-use)


## Install

```bash
git clone @todo
```

```bash
make network-up
```

```bash
make start
```

```bash
make install
```

## Makefile

**Bootstrap this project with:**

```bash
make start
```
> ^ To compose development container.

```bash
make stop
```
> ^ To compose down development container.

```bash
make build
```
> ^ To build image

```bash
make build-nocache
```
> ^ To build image without cache

```bash
make shell
```
> ^ To get a container shell bash

```bash
make clear
```
> ^ To clear container and image

```bash
make network-up
```
> ^ To create external network

```bash
make install
```
> ^ To install application before first run

```bash
make tests
```
> ^ To run tests


## How to use

### Buy endpoint
```
GET /api/v1/exchange/buy?from=EUR&to=GBP&amount=100"
```

Simple response:

```
{
    "currencyFrom": "EUR",
    "currencyTo": "GBP",
    "rate": 1.5678,
    "amount": 100,
    "amountExchanged": 158.3478,
    "provision": 1.5678
}
```

### Sell endpoint 

```
GET /api/v1/exchange/sell?from=EUR&to=GBP&amount=100"
```

Simple response:
200 successful operation
```
{
    "currencyFrom": "EUR",
    "currencyTo": "GBP",
    "rate": 1.5678,
    "amount": 100,
    "amountExchanged": 155.2122,
    "provision": 1.5678
}
```


### Sell endpoint, validation error

```
GET /api/v1/exchange/sell?from=&to=&amount=xyz"
```

Simple response:
400 validation error
```
{
    "message": "Validation error.",
    "errors": [
        {
            "from": {
                "message": "This value should not be blank.",
                "code": "c1051bb4-d103-4f74-8988-acbcafc7fdc3"
            }
        },
        {
            "to": {
                "message": "This value should not be blank.",
                "code": "c1051bb4-d103-4f74-8988-acbcafc7fdc3"
            }
        },
        {
            "amount": {
                "message": "This value should be of type numeric.",
                "code": "ba785a8c-82cb-4283-967c-3cf342181b40"
            }
        }
    ]
}
```