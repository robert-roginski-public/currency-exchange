DOCKER_COMPOSE_FILENAME=docker/local/docker-compose.yml
DOCKER_FILENAME=docker/local/Dockerfile
PROJECT_NAME=bertek-app
IMAGE_NAME=bertek-app
CONTAINER_NAME=bertek-app-php
NETWORK_NAME=bertek

start:
	docker compose -p ${PROJECT_NAME} -f ${DOCKER_COMPOSE_FILENAME} up -d

stop:
	docker compose -p ${PROJECT_NAME} -f ${DOCKER_COMPOSE_FILENAME} stop

build:
	docker build . -f ${DOCKER_FILENAME} -t ${IMAGE_NAME}

build-nocache:
	docker build . --no-cache -f ${DOCKER_FILENAME} -t ${IMAGE_NAME}

shell:
	docker exec -it ${CONTAINER_NAME} /bin/bash

clear:
	docker container rm ${CONTAINER_NAME}
	docker image rm ${IMAGE_NAME}

network-up:
	docker network create ${NETWORK_NAME}

install:
	echo "Installing..."
	docker exec -it ${CONTAINER_NAME} /bin/bash -c "cp docker/local/.env .env"
	docker exec -it ${CONTAINER_NAME} /bin/bash -c "cd /var/www && composer install"

test:
	echo "Testing ..."
	docker exec -it ${CONTAINER_NAME} /bin/bash -c "php bin/phpunit"
	docker exec -it ${CONTAINER_NAME} /bin/bash -c "vendor/bin/phpstan analyse -c phpstan.neon"