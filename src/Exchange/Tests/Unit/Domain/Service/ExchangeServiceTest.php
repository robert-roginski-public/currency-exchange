<?php

declare(strict_types=1);

namespace App\Exchange\Tests\Unit\Domain\Service;

use App\Exchange\Domain\Dto\CurrencyProvisionDto;
use App\Exchange\Domain\Dto\CurrencyRateDto;
use App\Exchange\Domain\Dto\ExchangeTransactionDto;
use App\Exchange\Domain\Enum\TransactionTypeEnum;
use App\Exchange\Domain\Exception\CurrencyRateNotExistsException;
use App\Exchange\Domain\Query\ExchangeTransactionQuery;
use App\Exchange\Domain\Repository\ExchangeRepositoryInterface;
use App\Exchange\Domain\Service\ExchangeService;
use PHPUnit\Framework\TestCase;

class ExchangeServiceTest extends TestCase
{
    private ExchangeRepositoryInterface $exchangeRepository;
    private ExchangeService $exchangeService;

    protected function setUp(): void
    {
        parent::setUp();

        $this->exchangeRepository = $this->createMock(ExchangeRepositoryInterface::class);
        $this->exchangeService = new ExchangeService($this->exchangeRepository);
    }

    public function testExchangeServiceCanCreate(): void
    {
        $this->assertInstanceOf(ExchangeService::class, $this->exchangeService);
    }

    public function testCurrencyRateNotExists(): void
    {
        $exchangeQuery = new ExchangeTransactionQuery('NONE', 'GBP', 100, TransactionTypeEnum::BUY);
        $this->exchangeRepository->method('getCurrentCurrencyRate')->willReturn(null);

        $this->expectException(CurrencyRateNotExistsException::class);

        ($this->exchangeService)($exchangeQuery);
    }

    public function testExchangeSell(): void
    {
        $exchangeQuery = new ExchangeTransactionQuery('EUR', 'GBP', 100, TransactionTypeEnum::SELL);
        $currencyRateDto = $this->getCurrencyRateDto($exchangeQuery);
        $currencyProvisionDto = $this->getCurrencyProvisionDto($exchangeQuery);

        $this->exchangeRepository->method('getCurrentCurrencyRate')->willReturn($currencyRateDto);
        $this->exchangeRepository->method('getCurrentProvision')->willReturn($currencyProvisionDto);

        $exchangeResultDto = ($this->exchangeService)($exchangeQuery);

        $this->assertInstanceOf(ExchangeTransactionDto::class, $exchangeResultDto);
        $this->assertEquals($exchangeQuery->currencyFrom, $exchangeResultDto->currencyFrom);
        $this->assertEquals($exchangeQuery->currencyTo, $exchangeResultDto->currencyTo);
        $this->assertEquals($currencyRateDto->rate, $exchangeResultDto->rate);
        $this->assertEquals($exchangeQuery->amount, $exchangeResultDto->amount);
        $this->assertEquals(198.0, $exchangeResultDto->amountExchanged);
        $this->assertEquals(2.0, $exchangeResultDto->provision);
    }

    public function testExchangeBuy(): void
    {
        $exchangeQuery = new ExchangeTransactionQuery('EUR', 'GBP', 100, TransactionTypeEnum::BUY);
        $currencyRateDto = $this->getCurrencyRateDto($exchangeQuery);
        $currencyProvisionDto = $this->getCurrencyProvisionDto($exchangeQuery);

        $this->exchangeRepository->method('getCurrentCurrencyRate')->willReturn($currencyRateDto);
        $this->exchangeRepository->method('getCurrentProvision')->willReturn($currencyProvisionDto);

        $exchangeResultDto = ($this->exchangeService)($exchangeQuery);

        $this->assertInstanceOf(ExchangeTransactionDto::class, $exchangeResultDto);
        $this->assertEquals($exchangeQuery->currencyFrom, $exchangeResultDto->currencyFrom);
        $this->assertEquals($exchangeQuery->currencyTo, $exchangeResultDto->currencyTo);
        $this->assertEquals($currencyRateDto->rate, $exchangeResultDto->rate);
        $this->assertEquals($exchangeQuery->amount, $exchangeResultDto->amount);
        $this->assertEquals(202.0, $exchangeResultDto->amountExchanged);
        $this->assertEquals(2.0, $exchangeResultDto->provision);
    }

    private function getCurrencyRateDto(ExchangeTransactionQuery $exchangeQuery): CurrencyRateDto
    {
        return new CurrencyRateDto(
            $exchangeQuery->currencyFrom, $exchangeQuery->currencyTo, 2
        );
    }

    private function getCurrencyProvisionDto(ExchangeTransactionQuery $exchangeQuery): CurrencyProvisionDto
    {
        return new CurrencyProvisionDto(
            $exchangeQuery->currencyFrom, $exchangeQuery->currencyTo, 1
        );
    }
}