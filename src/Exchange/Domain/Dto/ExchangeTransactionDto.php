<?php

declare(strict_types=1);

namespace App\Exchange\Domain\Dto;

class ExchangeTransactionDto
{
    public function __construct(
        public readonly string $currencyFrom,
        public readonly string $currencyTo,
        public readonly float $rate,
        public readonly float $amount,
        public readonly float $amountExchanged,
        public readonly float $provision
    ) {}
}