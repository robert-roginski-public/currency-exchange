<?php

declare(strict_types=1);

namespace App\Exchange\Domain\Dto;

class CurrencyProvisionDto
{
    public function __construct(
        public readonly string $currencyFrom,
        public readonly string $currencyTo,
        public readonly float $provision
    ) {}
}