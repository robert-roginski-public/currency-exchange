<?php

declare(strict_types=1);

namespace App\Exchange\Domain\Enum;

enum TransactionTypeEnum: string
{
    case BUY = 'buy';
    case SELL = 'sell';
}
