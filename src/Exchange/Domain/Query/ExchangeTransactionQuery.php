<?php

declare(strict_types=1);

namespace App\Exchange\Domain\Query;

use App\Exchange\Domain\Enum\TransactionTypeEnum;

class ExchangeTransactionQuery
{
    public function __construct(
        public readonly string $currencyFrom,
        public readonly string $currencyTo,
        public readonly float $amount,
        public readonly TransactionTypeEnum $type
    ) {}
}