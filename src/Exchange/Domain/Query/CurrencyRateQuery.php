<?php

declare(strict_types=1);

namespace App\Exchange\Domain\Query;

class CurrencyRateQuery
{
    public function __construct(
        public readonly string $currencyFrom,
        public readonly string $currencyTo,
    ) {}
}