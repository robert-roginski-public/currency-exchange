<?php

declare(strict_types=1);

namespace App\Exchange\Domain\Query;

class CurrencyProvisionQuery
{
    public function __construct(
        public readonly string $currencyFrom,
        public readonly string $currencyTo,
        public readonly float $amount
    ) {}
}