<?php

declare(strict_types=1);

namespace App\Exchange\Domain\Exception;

use Exception;

class CurrencyRateNotExistsException extends Exception
{
    protected $message = 'Current exchange rate Not Found for currencies "%s":"%s"';

    public function __construct(string $currencyFrom, string $currencyTo)
    {
        parent::__construct(sprintf($this->message, $currencyFrom, $currencyTo));
    }
}