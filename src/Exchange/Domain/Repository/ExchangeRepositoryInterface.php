<?php

declare(strict_types=1);

namespace App\Exchange\Domain\Repository;

use App\Exchange\Domain\Dto\CurrencyProvisionDto;
use App\Exchange\Domain\Dto\CurrencyRateDto;
use App\Exchange\Domain\Query\CurrencyProvisionQuery;
use App\Exchange\Domain\Query\CurrencyRateQuery;

interface ExchangeRepositoryInterface
{
    public function getCurrentCurrencyRate(CurrencyRateQuery $currencyRateQuery): ?CurrencyRateDto;

    public function getCurrentProvision(CurrencyProvisionQuery $currencyProvisionQuery): ?CurrencyProvisionDto;
}