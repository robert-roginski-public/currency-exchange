<?php

declare(strict_types=1);

namespace App\Exchange\Domain\Service;

use App\Exchange\Domain\Dto\CurrencyRateDto;
use App\Exchange\Domain\Dto\ExchangeTransactionDto;
use App\Exchange\Domain\Enum\TransactionTypeEnum;
use App\Exchange\Domain\Exception\CurrencyRateNotExistsException;
use App\Exchange\Domain\Query\CurrencyProvisionQuery;
use App\Exchange\Domain\Query\CurrencyRateQuery;
use App\Exchange\Domain\Query\ExchangeTransactionQuery;
use App\Exchange\Domain\Repository\ExchangeRepositoryInterface;

class ExchangeService
{
    public function __construct(private readonly ExchangeRepositoryInterface $repository){}

    public function __invoke(ExchangeTransactionQuery $query): ExchangeTransactionDto
    {
        $currentRate = $this->repository->getCurrentCurrencyRate(new CurrencyRateQuery(
            currencyFrom: $query->currencyFrom,
            currencyTo: $query->currencyTo,
        ));

        if (false === $currentRate instanceof CurrencyRateDto) {
            throw new CurrencyRateNotExistsException(
                currencyFrom: $query->currencyFrom,
                currencyTo: $query->currencyTo,
            );
        }

        $currentProvision = $this->repository->getCurrentProvision(new CurrencyProvisionQuery(
            currencyFrom: $query->currencyFrom,
            currencyTo: $query->currencyTo,
            amount: $query->amount
        ));

        $amountValue = $query->amount * $currentRate->rate;
        $provisionValue = $amountValue * ($currentProvision->provision/100);

        if (TransactionTypeEnum::SELL === $query->type) {
            $amountExchanged = $amountValue - $provisionValue;
        } else if (TransactionTypeEnum::BUY === $query->type) {
            $amountExchanged = $amountValue + $provisionValue;
        }

        return new ExchangeTransactionDto(
            currencyFrom: $query->currencyFrom,
            currencyTo: $query->currencyTo,
            rate: $currentRate->rate,
            amount: $query->amount,
            amountExchanged: $amountExchanged,
            provision: $provisionValue,
        );
    }
}