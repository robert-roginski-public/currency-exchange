<?php

declare(strict_types=1);

namespace App\Exchange\Application\Exception;

use Exception;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Throwable;

class ValidationException extends Exception
{
    private ConstraintViolationListInterface $violationList;

    public function __construct(ConstraintViolationListInterface $violationList, $code = 0, $message = "", Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);

        $this->violationList = $violationList;
    }

    public function getViolationList(): ConstraintViolationListInterface
    {
        return $this->violationList;
    }
}