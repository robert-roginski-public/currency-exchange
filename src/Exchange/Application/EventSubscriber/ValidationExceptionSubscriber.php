<?php

declare(strict_types=1);

namespace App\Exchange\Application\EventSubscriber;

use App\Exchange\Application\Exception\ValidationException;
use App\Exchange\Application\Serializer\Normalizer\ConstraintViolationListNormalizer;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;

class ValidationExceptionSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private readonly ConstraintViolationListNormalizer $violationListNormalizer
    ) {}

    public function onKernelException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();
        if (!($exception instanceof ValidationException)) {
            return;
        }

        $errors = $this->violationListNormalizer->normalize($exception);

        $event->setResponse(new JsonResponse($errors, Response::HTTP_BAD_REQUEST));
    }

    public static function getSubscribedEvents(): array
    {
        return [
            'kernel.exception' => 'onKernelException',
        ];
    }
}