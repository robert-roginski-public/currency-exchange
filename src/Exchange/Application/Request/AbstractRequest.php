<?php

declare(strict_types=1);

namespace App\Exchange\Application\Request;

use Symfony\Component\HttpFoundation\Request;

class AbstractRequest implements RequestInterface
{
    public function __construct(Request $request)
    {
        $payload = (array) json_decode($request->getContent(), true);
        $requestData = array_merge($payload, $request->query->all());

        foreach ($requestData as $key => $value) {
            if (property_exists($this, (string) $key) === true) {
                $this->$key = $value;
            }
        }
    }
}