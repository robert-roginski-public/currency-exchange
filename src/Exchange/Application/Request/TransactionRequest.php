<?php

declare(strict_types=1);

namespace App\Exchange\Application\Request;

use Symfony\Component\Validator\Constraints as Assert;

class TransactionRequest extends AbstractRequest
{
    #[Assert\NotBlank]
    protected mixed $from;

    #[Assert\NotBlank]
    protected mixed $to;

    #[Assert\NotBlank]
    #[Assert\Type('numeric')]
    protected mixed $amount;

    public function getFrom(): string
    {
        return $this->from;
    }

    public function getTo(): string
    {
        return $this->to;
    }

    public function getAmount(): float
    {
        return (float) $this->amount;
    }
}