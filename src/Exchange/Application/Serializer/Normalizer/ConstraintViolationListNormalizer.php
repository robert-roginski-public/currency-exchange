<?php

declare(strict_types=1);

namespace App\Exchange\Application\Serializer\Normalizer;

use App\Exchange\Application\Exception\ValidationException;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class ConstraintViolationListNormalizer
{
    public function normalize($object, string $format = null, array $context = array()): array
    {
        /** @var ValidationException $object */
        $violations = $this->getMessagesAndViolations($object->getViolationList());

        return [
            'message' => strlen($object->getMessage()) > 0 ? $object->getMessage() : 'Validation error.',
            'errors' => $violations,
        ];
    }

    public function supportsNormalization($data, string $format = null): bool
    {
        return $data instanceof ValidationException;
    }


    private function getMessagesAndViolations(ConstraintViolationListInterface $constraintViolationList): array
    {
        $violations = [];
        /** @var ConstraintViolation $violation */
        foreach ($constraintViolationList as $violation) {
            $violations[] = [
                $violation->getPropertyPath() => [
                    'message' => $violation->getMessage(),
                    'code' => $violation->getCode(),
                ],

            ];
        }
        return $violations;
    }
}