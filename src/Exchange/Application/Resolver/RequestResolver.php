<?php

declare(strict_types=1);

namespace App\Exchange\Application\Resolver;

use App\Exchange\Application\Exception\ValidationException;
use App\Exchange\Application\Request\RequestInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RequestResolver implements ValueResolverInterface
{
    public function __construct(
        protected readonly ValidatorInterface $validator,
    ) {}

    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $class = $argument->getType();
        if (false === is_a($class, RequestInterface::class, true)) {
            return [];
        }

        $requestClass = new $class($request);
        $errors = $this->validator->validate($requestClass);

        if (count($errors) > 0) {
            throw new ValidationException($errors);
        }

        return [$requestClass];
    }
}