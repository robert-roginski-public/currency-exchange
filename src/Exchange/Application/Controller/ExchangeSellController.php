<?php

declare(strict_types=1);

namespace App\Exchange\Application\Controller;

use App\Exchange\Application\Request\TransactionRequest;
use App\Exchange\Domain\Enum\TransactionTypeEnum;
use App\Exchange\Domain\Query\ExchangeTransactionQuery;
use App\Exchange\Domain\Service\ExchangeService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class ExchangeSellController extends AbstractController
{
    public function __construct(private readonly ExchangeService $exchangeService){}

    #[Route('/sell', name: 'sell')]
    public function __invoke(TransactionRequest $request): JsonResponse
    {
        $query = new ExchangeTransactionQuery(
            currencyFrom: $request->getFrom(),
            currencyTo: $request->getTo(),
            amount: $request->getAmount(),
            type: TransactionTypeEnum::SELL
        );

        $transactionDto = ($this->exchangeService)($query);

        return $this->json([
            'currencyFrom' => $transactionDto->currencyFrom,
            'currencyTo' => $transactionDto->currencyTo,
            'rate' => $transactionDto->rate,
            'amount' => $transactionDto->amount,
            'amountExchanged' => $transactionDto->amountExchanged,
            'provision' => $transactionDto->provision,
        ]);
    }
}