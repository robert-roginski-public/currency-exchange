<?php

declare(strict_types=1);

namespace App\Exchange\Infrastructure\Repository;

use App\Exchange\Domain\Dto\CurrencyProvisionDto;
use App\Exchange\Domain\Dto\CurrencyRateDto;
use App\Exchange\Domain\Query\CurrencyProvisionQuery;
use App\Exchange\Domain\Query\CurrencyRateQuery;
use App\Exchange\Domain\Repository\ExchangeRepositoryInterface;

class ExchangeRepository implements ExchangeRepositoryInterface
{
    private $currentRates = [
        'EUR' => ['GBP' => 1.5678],
        'GBP' => ['EUR' => 1.5432],
    ];

    public function getCurrentCurrencyRate(CurrencyRateQuery $query): ?CurrencyRateDto
    {
        if (false === empty($this->currentRates[$query->currencyFrom][$query->currencyTo])) {
            return new CurrencyRateDto(
                currencyFrom: $query->currencyFrom,
                currencyTo: $query->currencyTo,
                rate: $this->currentRates[$query->currencyFrom][$query->currencyTo],
            );
        }

        return null;
    }

    public function getCurrentProvision(CurrencyProvisionQuery $query): CurrencyProvisionDto
    {
        return new CurrencyProvisionDto(
            currencyFrom: $query->currencyFrom,
            currencyTo: $query->currencyTo,
            provision: 1,
        );
    }
}